Helm Chart
==========

Requirements
------------

You need to install :

* [kubectl](https://kubernetes.io/docs/tasks/tools/)
* [helm](https://helm.sh/docs/intro/install/)
* [k3d](https://k3d.io/stable/#installation)

Optionally, you can install :

* [kubectx](https://github.com/ahmetb/kubectx) if you want to switch easily between context and namespaces


Getting started
---------------

### DNS resolving
To access Ontopic Studio locally on http://ontopic.local/ should modify your hosts file with:

```bash
127.0.0.1  ontopic.local
```

### Create the cluster

```bash
k3d cluster create -c ontopic-cluster.yaml
```

### Create the namespace

```bash
kubectl create ns ontopic
kubens ontopic # if you have kubectx installed
# or
kubectl ns ontopic # if you installed it with krew
# or
kubectl-ns ontopic
```

### Create secret for registry

Use the provided username and password to pull the Ontopic Studio registry

```bash
# -n ontopic if you don't have kubens
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=$HOME/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson
# or
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com   --docker-username=<username>
--docker-password=<password>
```

### Deploy a postgresql database

Here is the command to deploy a postgresql database using the [bitnami helm chart](https://artifacthub.io/packages/helm/bitnami/postgresql).

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install store-server-db bitnami/postgresql --wait
```

Wait for the DB to be ready

### Create the database and users

```bash
kubectl exec -i store-server-db-postgresql-0 -- /opt/bitnami/scripts/postgresql/entrypoint.sh /bin/bash -c 'PGPASSWORD=$POSTGRES_PASSWORD psql' < create-db-and-users.sql
```

### Create the database password file
```bash
kubectl get secret store-server-db-postgresql -o jsonpath="{.data.postgres-password}" | base64 -d > ontopic-studio/database-password-file
```
### Create a user and set password as secret

Create the secret with the script, a new file with the chosen password will be generated in the new folder _identity_
```bash
./create-user.sh
```
Add the secret
```bash
kubectl create secret generic identity-password-db \
    --from-file=password-file-db=./identity/password-file-db
```

And then you add it in your values file :
```yaml
secrets:
  identity-password-db: /run/secrets/password-file-db
  client-secret: /run/secrets/client-secret
  cookie-secret: /run/secrets/cookie-secret
  azure-api-client-secret: /run/secrets/azure-api-client-secret
  okta-ssws-token: /run/secrets/okta-ssws-token
  keycloak-admin-password-file: /run/secrets/keycloak-admin-password-file
```

### Add the license as secret
Add the provided ontopic-studio license.

Create the secret:
```bash
kubectl create secret generic user-license-file \
    --from-file=user-license=../default-secrets/license/user-license
```

And then you add it in your values file :
```yaml
secrets:
  identity-password-db: /run/secrets/password-file-db
  client-secret: /run/secrets/client-secret
  cookie-secret: /run/secrets/cookie-secret
  azure-api-client-secret: /run/secrets/azure-api-client-secret
  okta-ssws-token: /run/secrets/okta-ssws-token
  keycloak-admin-password-file: /run/secrets/keycloak-admin-password-file
```
### Install the helm charts

Install the charts

```bash
helm install ontop-endpoint ontop-endpoint --wait

helm install -f values.yaml ontopic-studio ontopic-studio
watch kubectl get pod

```

You can access Ontopic Studio at http://ontopic.local:8080
